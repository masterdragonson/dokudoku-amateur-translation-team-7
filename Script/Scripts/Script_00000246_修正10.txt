class Window_Base < Window
	#draw_textの制御文字の事前変換するver
	#\cは仕様上最後に設定された色で全て描画
	#\Iは\wとかで勝手に付与されるので削除
	def draw_text_plus(*args)
		if args[0].is_a?(Rect)
			text = 1
		else
			text = 4
		end
		oc = Color.new()
		oc.set(contents.font.color)
		args[text] = convert_escape_characters(args[text])
		args[text].gsub!(/\eC\[(\d+)\]/i) {
			change_color(text_color($1.to_i))
			""
		}
		args[text].gsub!(/\eI\[(\d+)\]/i) {
			""
		}
		draw_text(*args)
		change_color(oc)
	end
end


class Game_Party < Game_Unit
	alias hima_library_update update
	def update
		hima_library_update
		set_library
	end
	def set_library
		all_items.map{|item| $game_library.get_item(item)}
	end
end


class IO
	alias hima_write write
	def write(*a)
		hima_write *a
	rescue SystemCallError=>ex
	end
end